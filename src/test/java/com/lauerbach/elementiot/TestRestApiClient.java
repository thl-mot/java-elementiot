package com.lauerbach.elementiot;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;

import org.junit.BeforeClass;
import org.junit.Test;

import com.lauerbach.elementiot.apiresources.DeviceInfo;
import com.lauerbach.elementiot.apiresources.Packet;
import com.lauerbach.elementiot.apiresources.Reading;
import com.lauerbach.elementiot.rest.RestApiClient;

public class TestRestApiClient {

	public static String device_1_id = "7e526cec-1e79-48ce-922d-da9be917c374";
	public static String device_1_name = "Zimmer-Tobi";

	static Properties properties = new Properties();

	@BeforeClass
	public static void init() throws FileNotFoundException {
		InputStream is = null;

		File f = new File( System.getProperty("user.home")+"/credential.properties");

		if (f.exists()) {
			is = new FileInputStream(f);
			try {
				System.out.println("using "+f.getAbsolutePath());
				properties.load(is);
				is.close();
				return;
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			System.out.println(f.getAbsolutePath()+" does not exist.");
		}

		is = TestRestApiClient.class.getClassLoader().getResourceAsStream(
				"public-credential.properties");
		System.out.println("using public-credential.properties");

		try {
			properties.load(is);
			is.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testDeviceInfo() throws ElementIoTApiException {
		RestApiClient client = new RestApiClient(properties);
		DeviceInfo deviceInfo = client.getDeviceInfo(device_1_id);
		System.out.println( deviceInfo.getInterfaces().get(0).getOpts().get("device_eui"));
		assertEquals(deviceInfo.getName(), device_1_name);
	}

	@Test
	public void testLastPacket() throws ElementIoTApiException {
		RestApiClient client = new RestApiClient(properties);
		List<Packet> r = client.getLastPacket(device_1_id);
		assertEquals(r.size(), 1);
	}

	@Test
	public void testLastReading() throws ElementIoTApiException {
		RestApiClient client = new RestApiClient(properties);
		List<Reading> r = client.getLastReading(device_1_id);
		assertEquals(r.size(), 1);
	}

}
