package com.lauerbach.elementiot;

import java.util.Properties;

public class ApiBase {
	
	public static final String ELEMENTIOT_API_KEY = "elementiot.api.key";
	public static final String ELEMENTIOT_API_HOST = "elementiot.api.host";
	
	protected String host;
	protected String apiKey;
	
	public ApiBase( Properties properties) {
		host= properties.getProperty(ELEMENTIOT_API_HOST);
		apiKey= properties.getProperty(ELEMENTIOT_API_KEY);
	}
	
	public ApiBase( String host, String key) {
		this.host= host;
		this.apiKey= key;
	}

	public String getHost() {
		return host;
	}

	public String getApiKey() {
		return apiKey;
	}

}
