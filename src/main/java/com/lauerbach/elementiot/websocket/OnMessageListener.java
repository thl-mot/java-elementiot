package com.lauerbach.elementiot.websocket;

public interface OnMessageListener<T> {

	public void onMessage( T endpointEvent);
	
}
