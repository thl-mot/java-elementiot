package com.lauerbach.elementiot.websocket;

import javax.websocket.ClientEndpoint;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.PongMessage;
import javax.websocket.Session;

import com.google.gson.Gson;

@ClientEndpoint
public class WebSocketEndpoint<T extends BaseEvent<?>> {
	OnMessageListener<T> listener = null;
	Class<?> clazz = null;
	private boolean pong;
	private long pongTime=0; 


	public WebSocketEndpoint(Class<?> clazz, OnMessageListener<T> listener) {
		this.listener = listener;
		this.clazz = clazz;
	}

	@OnOpen
	public void onOpen(Session session) {
	}

	@OnMessage
	public void processMessage(String message) {
		Gson gson = new Gson();
		if (listener != null) {
			@SuppressWarnings("unchecked")
			T[] events = (T[]) gson.fromJson(message, clazz);
			for (int i = 0; i < events.length; i++) {
				listener.onMessage(events[i]);
			}
		}

	}

	@OnMessage
	public synchronized void onPong(PongMessage message) {
		synchronized (this) {
			pong= true;
			pongTime= System.currentTimeMillis();
			try {
				this.notify();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}

	@OnError
	public void processError(Throwable t) {
		System.out.println("MyClientEndpoint Exception " + t.getMessage());
		t.printStackTrace();
	}

	public void resetPong() {
		this.pong= false;
	}
	
	public boolean isPong() {
		return this.pong;
	}

	public long getPongTime() {
		return pongTime;
	}

	
	
}
