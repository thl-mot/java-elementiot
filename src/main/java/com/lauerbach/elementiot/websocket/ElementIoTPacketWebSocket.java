package com.lauerbach.elementiot.websocket;

import java.util.Properties;

import com.lauerbach.elementiot.ElementIoTApiException;

public class ElementIoTPacketWebSocket extends ElementIoTBaseWebSocket<WebSocketEndpoint<PacketEvent>, PacketEvent> {

	public ElementIoTPacketWebSocket(
			Properties properties,
			com.lauerbach.elementiot.websocket.ElementIoTBaseWebSocket.FilterEnum filter,
			String id) {
		super(properties, filter, id);
	}

	public ElementIoTPacketWebSocket(Properties properties) {
		super(properties);
	}

	public ElementIoTPacketWebSocket(
			String host,
			String key,
			com.lauerbach.elementiot.websocket.ElementIoTBaseWebSocket.FilterEnum filter,
			String id) {
		super(host, key, filter, id);
	}

	public ElementIoTPacketWebSocket(String host, String key) {
		super(host, key);
	}

	@Override
	public void open(OnMessageListener<PacketEvent> listener)
			throws ElementIoTApiException {
		WebSocketEndpoint<PacketEvent> endpoint = new WebSocketEndpoint<PacketEvent>( PacketEvent[].class, listener);
		open(listener, "readings", endpoint);
	}

}
