package com.lauerbach.elementiot.websocket;

import java.util.Properties;

import com.lauerbach.elementiot.ElementIoTApiException;

public class ElementIoTReadingWebSocket extends ElementIoTBaseWebSocket<WebSocketEndpoint<ReadingEvent>, ReadingEvent> {

	public ElementIoTReadingWebSocket(Properties properties,
			com.lauerbach.elementiot.websocket.ElementIoTBaseWebSocket.FilterEnum filter, String id) {
		super(properties, filter, id);
	}

	public ElementIoTReadingWebSocket(Properties properties) {
		super(properties);
	}

	public ElementIoTReadingWebSocket(String host, String key,
			com.lauerbach.elementiot.websocket.ElementIoTBaseWebSocket.FilterEnum filter, String id) {
		super(host, key, filter, id);
	}

	public ElementIoTReadingWebSocket(String host, String key) {
		super(host, key);
	}

	public void open(OnMessageListener<ReadingEvent> listener) throws ElementIoTApiException {
		WebSocketEndpoint<ReadingEvent> endpoint = new WebSocketEndpoint<ReadingEvent>(ReadingEvent[].class, listener);
		open(listener, "readings", endpoint);
	}

	public static void main(String[] args) {
		ElementIoTReadingWebSocket con = new ElementIoTReadingWebSocket("element-iot.com",
				"f9234f7cdf2cbec145aaee4222682900", FilterEnum.BY_TAG, "2e9bfef5-29b0-48b2-95a1-ffaccd9900fa");
		try {
			con.open(new OnMessageListener<ReadingEvent>() {
				public void onMessage(ReadingEvent endpointEvent) {
					System.out
							.println(endpointEvent.getBody().getDevice_id() + " " + endpointEvent.getBody().getData());
				}
			});
		} catch (Exception ex) {
			System.err.println("Unable to connect to server.");
		}
		while (true) {
			Long time = con.ping();
			if (time == null || time > 4000) {
				try {
					con.reconnect();
				} catch (Exception ex) {
					System.err.println("Unable to reconnect to server.");
				}
			}
			try {
				Thread.sleep(1000 * 60);
			} catch (InterruptedException e) {
				break;
			}
		}
	}

}
