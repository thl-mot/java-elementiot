package com.lauerbach.elementiot.websocket;

import java.io.IOException;
import java.net.URI;
import java.nio.ByteBuffer;
import java.util.Properties;

import javax.websocket.ContainerProvider;
import javax.websocket.Session;
import javax.websocket.WebSocketContainer;

import com.lauerbach.elementiot.ApiBase;
import com.lauerbach.elementiot.ElementIoTApiException;

public abstract class ElementIoTBaseWebSocket<ENDPOINT_TYPE extends WebSocketEndpoint<?>, EVENT_TYPE extends BaseEvent<?>> extends ApiBase {
	public enum FilterEnum {
		BY_DEVICE, BY_TAG, BY_MANDATE
	}

	private FilterEnum filter;
	private String id;
	private long maxIdleTimeout = 0; // default is endless

	private Session session;
	private ENDPOINT_TYPE endpoint;
	private String serverUri = null;
	private WebSocketContainer container;
	
	private long pingTime=0; 
	private long lastPingTime=0;
	

	public ElementIoTBaseWebSocket(Properties properties) {
		super(properties);
		filter = FilterEnum.BY_MANDATE;
		this.id = null;
	}

	public ElementIoTBaseWebSocket(Properties properties, FilterEnum filter, String id) {
		super(properties);
		this.filter = filter;
		this.id = id;
	}

	public ElementIoTBaseWebSocket(String host, String key) {
		super(host, key);
		this.filter = FilterEnum.BY_MANDATE;
		this.id = null;
	}

	public ElementIoTBaseWebSocket(String host, String key, FilterEnum filter,
			String id) {
		super(host, key);
		this.filter = filter;
		this.id = id;
	}

	public void reconnect() throws ElementIoTApiException {
		if (session != null && session.isOpen()) {
			try {
				session.close();
			} catch (IOException e) {
				throw new ElementIoTApiException("", e);
			}
		}
		session= null;
		try {
			session = container.connectToServer(endpoint, URI.create(serverUri));
			session.setMaxIdleTimeout(maxIdleTimeout);
			System.out.printf("Connected to : %s%n", serverUri);
		} catch (Exception e) {
			throw new ElementIoTApiException("Failed to connect to "+serverUri, e);
		}
	}

	protected void open(OnMessageListener<EVENT_TYPE> listener,
			String artefactName, ENDPOINT_TYPE endpoint) throws ElementIoTApiException {
		String subUrl = "";
		switch (filter) {
		case BY_DEVICE:
			subUrl = "/v1/devices/" + id + "/" + artefactName + "/socket";
			break;
		case BY_TAG:
			subUrl = "/v1/tags/" + id + "/" + artefactName + "/socket";
			break;
		case BY_MANDATE:
			subUrl = "/v1/" + artefactName + "/socket";
			break;
		}

		serverUri = "wss://" + host + "/api" + subUrl + "?auth=" + apiKey;
		this.endpoint= endpoint;
		container = ContainerProvider.getWebSocketContainer();
		session = null;
		try {
			reconnect();
		} catch (Exception e) {
			throw new ElementIoTApiException("Unable to (re-)connect", e);
		}

	}

	public abstract  void open(OnMessageListener<EVENT_TYPE> listener)
			throws ElementIoTApiException;

	public Long ping() {
		return ping(5000);
	}

	public Long ping(int timeout) {
		if (session == null || !session.isOpen()) {
			return null;
		}

		synchronized (endpoint) {
			endpoint.resetPong();
			
			ByteBuffer bb = ByteBuffer.allocate(8);
			bb.putLong(0, System.currentTimeMillis());
			try {
				pingTime= System.currentTimeMillis();
				session.getBasicRemote().sendPing(bb);
				System.out.println("Ping...");
				endpoint.wait(timeout);
				if (endpoint.isPong()) {
					lastPingTime= (endpoint.getPongTime()-pingTime);
					System.out.println("Pong ("+endpoint.isPong()+","+lastPingTime+")");
					return lastPingTime;
				} else {
					System.out.println("no Pong");
					lastPingTime= 0;
					return null;
				}
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
				return null;
			} catch (IOException e) {
				e.printStackTrace();
				return null;
			} catch (InterruptedException e) {
				e.printStackTrace();
				return null;
			}
		}
	}

	public void close() throws IOException {
		if (session != null && session.isOpen()) {
			session.close();
		}
	}

	public boolean isRunning() {
		return session != null && session.isOpen();
	}

	public long getMaxIdleTimeout() {
		return maxIdleTimeout;
	}

	public void setMaxIdleTimeout(long maxIdleTimeout) {
		this.maxIdleTimeout = maxIdleTimeout;
	}

	public FilterEnum getFilter() {
		return filter;
	}

	public String getId() {
		return id;
	}

	public long getLastPingTime() {
		return lastPingTime;
	}

}
