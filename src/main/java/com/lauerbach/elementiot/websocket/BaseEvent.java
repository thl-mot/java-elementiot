package com.lauerbach.elementiot.websocket;

public class BaseEvent<TYPE> {
	TYPE body;
	String event;

	public TYPE getBody() {
		return body;
	}

	public void setBody(TYPE body) {
		this.body = body;
	}

	public String getEvent() {
		return event;
	}

	public void setEvent(String event) {
		this.event = event;
	}

}
