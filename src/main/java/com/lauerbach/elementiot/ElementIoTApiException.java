package com.lauerbach.elementiot;

public class ElementIoTApiException extends Exception {

	private static final long serialVersionUID = 1350949204247575731L;

	public ElementIoTApiException(String message) {
		super(message);
	}

	public ElementIoTApiException(String msg, Exception e) {
		super( msg, e);
	}
	

}
