package com.lauerbach.elementiot.rest;

public class Response<TYPE> {
	int status;
	boolean ok;
	TYPE body;
	
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public boolean isOk() {
		return ok;
	}
	public void setOk(boolean ok) {
		this.ok = ok;
	}
	public TYPE getBody() {
		return body;
	}
	public void setBody(TYPE body) {
		this.body = body;
	}
	
	
}
