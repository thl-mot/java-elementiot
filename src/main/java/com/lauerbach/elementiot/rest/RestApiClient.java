package com.lauerbach.elementiot.rest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import com.google.gson.Gson;
import com.lauerbach.elementiot.ApiBase;
import com.lauerbach.elementiot.ElementIoTApiException;
import com.lauerbach.elementiot.apiresources.DeviceInfo;
import com.lauerbach.elementiot.apiresources.Packet;
import com.lauerbach.elementiot.apiresources.Reading;

public class RestApiClient extends ApiBase {

	public RestApiClient(Properties properties) {
		super(properties);

	}

	public RestApiClient(String host, String key) {
		super(host, key);
	}

	private <T extends Response<?>> T request(String urlString,
			HashMap<String, String> params, Class<T> classOfT)
			throws IOException, ElementIoTApiException {

		StringBuffer paramsString = new StringBuffer("?auth=" + apiKey);

		if (params != null) {
			Iterator<String> i = params.keySet().iterator();
			while (i.hasNext()) {
				String k = i.next();
				String v = params.get(k);
				paramsString.append("&" + k + "=" + v);
			}
		}

		URL url = new URL("https://" + host + "/api" + urlString
				+ paramsString.toString());

		System.out.println("Request: " + url.toString());
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setDoOutput(true);
		conn.setRequestMethod("GET");
		conn.setRequestProperty("Accept", "application/json");

		if (conn.getResponseCode() != 200) {
			throw new RuntimeException("Failed : HTTP error code : "
					+ conn.getResponseCode());
		}

		BufferedReader streamReader = new BufferedReader(new InputStreamReader(
				(conn.getInputStream())));

		StringBuilder responseStrBuilder = new StringBuilder();

		String inputStr;
		while ((inputStr = streamReader.readLine()) != null) {
			responseStrBuilder.append(inputStr);
		}
		streamReader.close();

		Gson gson = new Gson();

		T response = gson.fromJson(responseStrBuilder.toString(), classOfT);
		if (response.getStatus() != 200) {
			throw new ElementIoTApiException("Service responded with status "
					+ response.getStatus());
		}

		return response;
	}

	public DeviceInfo getDeviceInfo(String id) throws ElementIoTApiException {
		try {
			return request("/v1/devices/" + id, null, DeviceInfoResponse.class)
					.getBody();
		} catch (IOException e) {
			throw new ElementIoTApiException("Unable to execute API", e);
		}

	}

	public List<Packet> getLastPacket(String id) throws ElementIoTApiException {
		return getLastPacket(id, 1);
	}

	public List<Packet> getLastPacket(String id, int limit)
			throws ElementIoTApiException {
		HashMap<String, String> params = new HashMap<String, String>();
		params.put("limit", limit + "");
		params.put("sort", "inserted_at");
		params.put("sort_direction", "desc");
		try {
			return request("/v1/devices/" + id + "/packets", params,
					PacketResponse.class).getBody();
		} catch (IOException e) {
			throw new ElementIoTApiException("Unable to execute API", e);
		}
	}

	public List<Reading> getLastReading(String id) throws ElementIoTApiException {
		return getLastReading(id, 1);
	}

	public List<Reading> getLastReading(String id, int limit)
			throws ElementIoTApiException {
		HashMap<String, String> params = new HashMap<String, String>();
		params.put("limit", limit + "");
		params.put("sort", "inserted_at");
		params.put("sort_direction", "desc");
		try {
			return request("/v1/devices/" + id + "/readings", params,
					ReadingResponse.class).getBody();
		} catch (IOException e) {
			throw new ElementIoTApiException("Unable to execute API", e);
		}
	}

}
