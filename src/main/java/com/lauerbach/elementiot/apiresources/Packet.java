package com.lauerbach.elementiot.apiresources;

import java.util.Date;
import java.util.List;

public class Packet {
    Date transceived_at;
    String payload_encoding;
    String payload;
    String packet_type;
    Meta meta;
	boolean is_meta;
    String interface_id;
    String inserted_at;
    String id;
    String device_id;
    List<Gateway> gateways;
    
	public Date getTransceived_at() {
		return transceived_at;
	}
	public void setTransceived_at(Date transceived_at) {
		this.transceived_at = transceived_at;
	}
	public String getPayload_encoding() {
		return payload_encoding;
	}
	public void setPayload_encoding(String payload_encoding) {
		this.payload_encoding = payload_encoding;
	}
	public String getPayload() {
		return payload;
	}
	public void setPayload(String payload) {
		this.payload = payload;
	}
	public String getPacket_type() {
		return packet_type;
	}
	public void setPacket_type(String packet_type) {
		this.packet_type = packet_type;
	}
	public Meta getMeta() {
		return meta;
	}
	public void setMeta(Meta meta) {
		this.meta = meta;
	}
	public boolean isIs_meta() {
		return is_meta;
	}
	public void setIs_meta(boolean is_meta) {
		this.is_meta = is_meta;
	}
	public String getInterface_id() {
		return interface_id;
	}
	public void setInterface_id(String interface_id) {
		this.interface_id = interface_id;
	}
	public String getInserted_at() {
		return inserted_at;
	}
	public void setInserted_at(String inserted_at) {
		this.inserted_at = inserted_at;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getDevice_id() {
		return device_id;
	}
	public void setDevice_id(String device_id) {
		this.device_id = device_id;
	}
    
}
