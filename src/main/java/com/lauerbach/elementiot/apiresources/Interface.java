package com.lauerbach.elementiot.apiresources;

import java.util.HashMap;

public class Interface {
	HashMap<String, String> opts;
	Meta meta;
	String id;
	boolean enabled;
	String driver_instance_id;
	String device_id;

	public HashMap<String, String> getOpts() {
		return opts;
	}

	public void setOpts(HashMap<String, String> opts) {
		this.opts = opts;
	}

	public Meta getMeta() {
		return meta;
	}

	public void setMeta(Meta meta) {
		this.meta = meta;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getDriver_instance_id() {
		return driver_instance_id;
	}

	public void setDriver_instance_id(String driver_instance_id) {
		this.driver_instance_id = driver_instance_id;
	}

	public String getDevice_id() {
		return device_id;
	}

	public void setDevice_id(String device_id) {
		this.device_id = device_id;
	}

}

// "opts": {
// "rx_delay": 1,
// "rx2_dr": 0,
// "region": "EU863",
// "network_session_key": "C0F61CB4B5E6454741057D3ABCC58DA7",
// "join_eui": "0000000000000000",
// "gw_whitelist": null,
// "device_type": "otaa",
// "device_key": "2B7E151628AED2A6ABF7158809CF4F3C",
// "device_eui": "A81758FFFE03F35D",
// "device_address": "018654B6",
// "class_c": false,
// "check_fcnt": true,
// "app_session_key": "42D8D1CFEE85EF5CF984181B5D320AAE"
// },
// "meta": null,
// "id": "db4d3e32-d162-4809-a8b5-db14eaa2d7d8",
// "enabled": true,
// "driver_instance_id": "6bd2b64d-7cc4-4ca8-9d6b-baa162b040e9",
// "device_id": "7e526cec-1e79-48ce-922d-da9be917c374"