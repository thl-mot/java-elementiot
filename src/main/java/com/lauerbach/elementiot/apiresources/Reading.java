package com.lauerbach.elementiot.apiresources;

import java.util.Date;
import java.util.HashMap;

public class Reading {
	String parser_id;
	String packet_id;
	String measured_at;
	// location": null,
	Date inserted_at;
	String id;
	String device_id;
	HashMap<String, Object> data;
	public String getParser_id() {
		return parser_id;
	}
	public void setParser_id(String parser_id) {
		this.parser_id = parser_id;
	}
	public String getPacket_id() {
		return packet_id;
	}
	public void setPacket_id(String packet_id) {
		this.packet_id = packet_id;
	}
	public String getMeasured_at() {
		return measured_at;
	}
	public void setMeasured_at(String measured_at) {
		this.measured_at = measured_at;
	}
	public Date getInserted_at() {
		return inserted_at;
	}
	public void setInserted_at(Date inserted_at) {
		this.inserted_at = inserted_at;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getDevice_id() {
		return device_id;
	}
	public void setDevice_id(String device_id) {
		this.device_id = device_id;
	}
	public HashMap<String, Object> getData() {
		return data;
	}
	public void setData(HashMap<String, Object> data) {
		this.data = data;
	}
	
	
}
