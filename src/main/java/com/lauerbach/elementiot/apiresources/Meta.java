package com.lauerbach.elementiot.apiresources;

import java.util.List;

public class Meta {
	int stat;
	int size;
	// Object rx;
	int rfch;
	RegionMeta region_meta;
	String region;
	// Object powe;
	String modu;
	List<MacCommand> mac_commands;
	double lorawan_toa_ms;
	// Object ipol;
	List<GatewayStat> gateway_stats;
	double frequency;
	int frame_port;
	int frame_count_up;
	// Object dev_nonce;
	String datr;
	int data_rate;
	boolean confirm;
	String codr;
	int chan;
	boolean adr_ack_req;
	boolean ack;
	
}
