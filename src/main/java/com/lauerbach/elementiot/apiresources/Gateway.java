package com.lauerbach.elementiot.apiresources;

import java.util.Date;

public class Gateway {

	long tmst;
	int snr;
	int rssi;
	String router_id_hex;
	String router_id;
	Date updated_at;
	String template_id;
	boolean static_location;
	String slug;
	String parser_id;
	String name;
	// meta: {...}
	String mandate_id;
	String location;
	// last_readings: [...]
	Date inserted_at;
	String id;
	// icon: devices
	String default_readings_view_id;
	String default_packets_view_id;
	String default_layers_id;
	String default_graph_preset_id;
	public long getTmst() {
		return tmst;
	}
	public void setTmst(long tmst) {
		this.tmst = tmst;
	}
	public int getSnr() {
		return snr;
	}
	public void setSnr(int snr) {
		this.snr = snr;
	}
	public int getRssi() {
		return rssi;
	}
	public void setRssi(int rssi) {
		this.rssi = rssi;
	}
	public String getRouter_id_hex() {
		return router_id_hex;
	}
	public void setRouter_id_hex(String router_id_hex) {
		this.router_id_hex = router_id_hex;
	}
	public String getRouter_id() {
		return router_id;
	}
	public void setRouter_id(String router_id) {
		this.router_id = router_id;
	}
	public Date getUpdated_at() {
		return updated_at;
	}
	public void setUpdated_at(Date updated_at) {
		this.updated_at = updated_at;
	}
	public String getTemplate_id() {
		return template_id;
	}
	public void setTemplate_id(String template_id) {
		this.template_id = template_id;
	}
	public boolean isStatic_location() {
		return static_location;
	}
	public void setStatic_location(boolean static_location) {
		this.static_location = static_location;
	}
	public String getSlug() {
		return slug;
	}
	public void setSlug(String slug) {
		this.slug = slug;
	}
	public String getParser_id() {
		return parser_id;
	}
	public void setParser_id(String parser_id) {
		this.parser_id = parser_id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getMandate_id() {
		return mandate_id;
	}
	public void setMandate_id(String mandate_id) {
		this.mandate_id = mandate_id;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public Date getInserted_at() {
		return inserted_at;
	}
	public void setInserted_at(Date inserted_at) {
		this.inserted_at = inserted_at;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getDefault_readings_view_id() {
		return default_readings_view_id;
	}
	public void setDefault_readings_view_id(String default_readings_view_id) {
		this.default_readings_view_id = default_readings_view_id;
	}
	public String getDefault_packets_view_id() {
		return default_packets_view_id;
	}
	public void setDefault_packets_view_id(String default_packets_view_id) {
		this.default_packets_view_id = default_packets_view_id;
	}
	public String getDefault_layers_id() {
		return default_layers_id;
	}
	public void setDefault_layers_id(String default_layers_id) {
		this.default_layers_id = default_layers_id;
	}
	public String getDefault_graph_preset_id() {
		return default_graph_preset_id;
	}
	public void setDefault_graph_preset_id(String default_graph_preset_id) {
		this.default_graph_preset_id = default_graph_preset_id;
	}

	
	
}
