package com.lauerbach.elementiot.apiresources;

import java.util.Date;
import java.util.List;

public class DeviceInfo {
	Date updated_at;
	String template_id;
	List<Tag> tags;
	boolean static_location;
	String slug;
	List<ProfileData> profile_data;
	String parser_id;
	String name;
	Object meta;
	String mandate_id;
	Object location;
	List<Packet> last_reading;
	List<Interface> interfaces;
	Date inserted_at;
	String id;
	String icon;
	Fields fields;
	String default_readings_view_id;
	String default_packets_view_id;
	String default_layers_id;
	String default_graph_preset_id;
	public Date getUpdated_at() {
		return updated_at;
	}
	public void setUpdated_at(Date updated_at) {
		this.updated_at = updated_at;
	}
	public String getTemplate_id() {
		return template_id;
	}
	public void setTemplate_id(String template_id) {
		this.template_id = template_id;
	}
	public List<Tag> getTags() {
		return tags;
	}
	public void setTags(List<Tag> tags) {
		this.tags = tags;
	}
	public boolean isStatic_location() {
		return static_location;
	}
	public void setStatic_location(boolean static_location) {
		this.static_location = static_location;
	}
	public String getSlug() {
		return slug;
	}
	public void setSlug(String slug) {
		this.slug = slug;
	}
	public List<ProfileData> getProfile_data() {
		return profile_data;
	}
	public void setProfile_data(List<ProfileData> profile_data) {
		this.profile_data = profile_data;
	}
	public String getParser_id() {
		return parser_id;
	}
	public void setParser_id(String parser_id) {
		this.parser_id = parser_id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Object getMeta() {
		return meta;
	}
	public void setMeta(Object meta) {
		this.meta = meta;
	}
	public String getMandate_id() {
		return mandate_id;
	}
	public void setMandate_id(String mandate_id) {
		this.mandate_id = mandate_id;
	}
	public Object getLocation() {
		return location;
	}
	public void setLocation(Object location) {
		this.location = location;
	}
	public List<Packet> getLast_reading() {
		return last_reading;
	}
	public void setLast_reading(List<Packet> last_reading) {
		this.last_reading = last_reading;
	}
	public List<Interface> getInterfaces() {
		return interfaces;
	}
	public void setInterfaces(List<Interface> interfaces) {
		this.interfaces = interfaces;
	}
	public Date getInserted_at() {
		return inserted_at;
	}
	public void setInserted_at(Date inserted_at) {
		this.inserted_at = inserted_at;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	public Fields getFields() {
		return fields;
	}
	public void setFields(Fields fields) {
		this.fields = fields;
	}
	public String getDefault_readings_view_id() {
		return default_readings_view_id;
	}
	public void setDefault_readings_view_id(String default_readings_view_id) {
		this.default_readings_view_id = default_readings_view_id;
	}
	public String getDefault_packets_view_id() {
		return default_packets_view_id;
	}
	public void setDefault_packets_view_id(String default_packets_view_id) {
		this.default_packets_view_id = default_packets_view_id;
	}
	public String getDefault_layers_id() {
		return default_layers_id;
	}
	public void setDefault_layers_id(String default_layers_id) {
		this.default_layers_id = default_layers_id;
	}
	public String getDefault_graph_preset_id() {
		return default_graph_preset_id;
	}
	public void setDefault_graph_preset_id(String default_graph_preset_id) {
		this.default_graph_preset_id = default_graph_preset_id;
	}
	
	
}
